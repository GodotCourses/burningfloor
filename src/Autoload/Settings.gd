extends Node

signal ads_status_changed(status)

var enable_sound: = true
var enable_music: = true
var enable_ads: = true setget set_enable_ads

var score_file: = "user://data/highscore.dat"
var settings_file: = "user://data/settings.dat"

var circles_per_level: = 5

var color_schemes = {
	"NEON1":
	{
		'background': Color8(50, 50, 70),
		'player_body': Color8(203, 255, 0),
		'player_trail': Color8(204, 0, 255),
		'circle_fill': Color8(255, 0, 110),
		'circle_static': Color8(0, 255, 102),
		'circle_limited': Color8(204, 0, 255)
	},
	"NEON2":
	{
		'background': Color8(0, 0, 0),
		'player_body': Color8(246, 255, 0),
		'player_trail': Color8(255, 255, 255),
		'circle_fill': Color8(255, 0, 110),
		'circle_static': Color8(151, 255, 48),
		'circle_limited': Color8(127, 0, 255)
	},
	"NEON3":
	{
		'background': Color8(76, 84, 95),
		'player_body': Color8(255, 0, 187),
		'player_trail': Color8(255, 148, 0),
		'circle_fill': Color8(255, 148, 0),
		'circle_static': Color8(170, 255, 0),
		'circle_limited': Color8(204, 0, 255)
	}
}
var theme = color_schemes["NEON1"]

func _ready() -> void:
	load_settings()


# Returns a random integer value using an array of weights for each value
# example :
#   rand_weighted ([18,2])
#   will return 0 on a 18/20 chance (90%)
#   will return 1 on a 2/20 chance (10%)
static func rand_weighted(weights) -> int:
	if !weights || weights.size() < 1:
		return 0
	var sum: = 0
	for weight in weights:
		sum += weight
	var num = rand_range(0, sum)
	for i in weights.size():
		if num < weights[i]:
			return i
		num -= weights[i]
	return 0  # never executed, but necessary for avoid an error check


func set_enable_ads(value: bool) -> void:
	var old_value: = enable_ads
	enable_ads = value
	if old_value != enable_ads:
		emit_signal('ads_status_changed', enable_ads)


func save_settings() -> void:
	var f: = File.new()
	if OK == f.open(Settings.settings_file, File.WRITE):
		f.store_8(enable_sound)
		f.store_8(enable_music)
		f.store_8(enable_ads)
		f.close()


func load_settings() -> void:
	var f: = File.new()
	if f.file_exists(Settings.settings_file):
		if OK == f.open(Settings.settings_file, File.READ):
			enable_sound = bool(f.get_8())
			enable_music = bool(f.get_8())
			self.enable_ads = bool(f.get_8())
			f.close()
		else:
			print("Error opening settings save file:%s"%Settings.settings_file)
