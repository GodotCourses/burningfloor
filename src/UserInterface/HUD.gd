extends CanvasLayer

var score_tween: = 0


func _on_Tween_tween_step(_object: Object, _key: NodePath, _elapsed: float, value: int) -> void:
	$ScoreBox/HBoxContainer/Score.text = str(value)


func _ready() -> void:
	$Message.rect_pivot_offset = $Message.rect_size/2


func show_message(text: String) -> void:
	$Message.text = text
	$MessageAnimation.play('show_message')


func hide() -> void:
	$ScoreBox.hide()
	$TopBox.hide()


func show() -> void:
	$ScoreBox.show()
	$TopBox.show()


func update_score(score: int, value: int) -> void:
	$Tween.interpolate_property(self, "score_tween", score, value, 0.25, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$Tween.start()
	if value > 1:
		$ScoreAnimation.play('score')


func update_bonus(value: int) -> void:
	$TopBox/HBoxContainer/Bonus.text = str(value) + 'x'
	if value > 1:
		$BonusAnimation.play('bonus')
