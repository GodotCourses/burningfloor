class_name Circle
extends Area2D

signal orbit_completed

enum MODES { STATIC, LIMITED }

onready var orbit_position: = $Pivot/OrbitPosition
onready var move_tween: = $MoveTween

var radius: = 120
var rotation_speed: = 4
var mode: int = MODES.STATIC
var num_orbits: = 3
var current_orbits: = 0
var orbit_start: = 0
var jumper: Node2D = null  # not possible to use Jumper as parameter type due to cyclic dependency
var move_range: = 0
var move_speed: = 0.0


func _draw() -> void:
	if mode != MODES.LIMITED:
		return
	if jumper:
		# warning-ignore:integer_division
		var r: int = ((radius - 50)/num_orbits)*(1 + current_orbits)
		draw_circle_arc_poly(Vector2.ZERO, r + 10, orbit_start + PI/2, $Pivot.rotation + PI/2, Settings.theme['circle_fill'])


func _process(delta: float) -> void:
	$Pivot.rotation += rotation_speed*delta
	if jumper:
		check_orbits()
		update() # necessary to make _draw() function called


func init(_position: Vector2, level: = 1) -> void:
	# adjust some values with difficulty level

	# chance to have a STATIC or LIMITED mode
	# level=1: _mode=0 (STATIC) -> 100% of chance
	# level=6: _mode=0 (STATIC) -> 10/15 (66%) of chance
	# level=11: _mode=0 (STATIC) -> 10/20 (50%) of chance
	# level=21: _mode=0 (STATIC) -> 10/30 (33%) of chance
	var _mode = Settings.rand_weighted([10, level - 1])
	set_mode(_mode)

	# chance to move or not
	# level=1: 0/10 -> 0% of chance
	# level=11: 1/10 -> 10% of chance
	# level=20+: 9/10 -> 90% of chance
	var move_chance = clamp(level - 10, 0, 9)/10.0
	if randf() < move_chance:
		# if moving, we get random values for range and speed move
		# betwwen minimal and maximal values, modified by the level
		# warning-ignore:narrowing_conversion
		move_range = max(25, 100*rand_range(0.75, 1.25)*move_chance)*pow(-1, randi()%2)
		move_speed = max(2.5 - ceil(level/5.0)*0.25, 0.75)

	# chance to have a smaller size than default one
	var small_chance = min(0.9, max(0, (level - 10)/20.0))
	if randf() < small_chance:
		# warning-ignore:narrowing_conversion
		radius = max(50, radius - level*rand_range(0.75, 1.25))

	position = _position
	# duplicate the material to allow each circle to have its proper material (same as "make unique" in the inspector)
	$Sprite.material = $Sprite.material.duplicate()
	$SpriteEffect.material = $Sprite.material
	# duplicate the shape to allow each circle to have its proper shape (same as "make unique" in the inspector)
	$CollisionShape2D.shape = $CollisionShape2D.shape.duplicate()
	$CollisionShape2D.shape.radius = radius
	var img_size: = ($Sprite.texture.get_size().x/2) as float
	$Sprite.scale = Vector2(1, 1)*radius/img_size
	orbit_position.position.x = radius
	if randf() > .5:
		rotation_speed *= -1
	set_tween()


func implode() -> void:
	jumper = null
	$AnimationPlayer.play('implode')
	yield ($AnimationPlayer, 'animation_finished')
	queue_free()


# not possible to use Jumper type (class) as parameter type due to cyclic dependency
func capture(_jumper: Node2D) -> void:
	current_orbits = 0
	jumper = _jumper
	if jumper:
		$Pivot.rotation = (jumper.position - position).angle()
		orbit_start = $Pivot.rotation
	$AnimationPlayer.play('capture')


func set_mode(_mode: = MODES.STATIC) -> void:
	mode = _mode
	var color: Color

	match mode:
		MODES.STATIC:
			color = Settings.theme['circle_static']
			$Label.hide()
		MODES.LIMITED:
			color = Settings.theme['circle_limited']
			$Label.text = str(num_orbits)
			$Label.show()
	$Sprite.material.set_shader_param('color', color)


func check_orbits() -> void:
	# do we make a full circle rotation ?
	if abs($Pivot.rotation - orbit_start) > 2*PI:
		current_orbits += 1
		emit_signal('orbit_completed')
		if MODES.LIMITED == mode:
			if Settings.enable_sound:
				$Beep.play()
			$Label.text = str(num_orbits - current_orbits)
			if current_orbits >= num_orbits:
				jumper.die()
				jumper = null
				implode()
		orbit_start = $Pivot.rotation


func draw_circle_arc_poly(center: Vector2, _radius: int, angle_from: float, angle_to: float, color: Color) -> void:
	# more info: https://docs.godotengine.org/en/3.1/tutorials/2d/custom_drawing_in_2d.html
	var nb_points: = 32
	var points_arc: = PoolVector2Array()
	points_arc.push_back(center)
	var colors: = PoolColorArray([color])

	for i in range(nb_points + 1):
		var angle_point: = angle_from + i*(angle_to - angle_from)/nb_points - PI/2
		points_arc.push_back(center + Vector2(cos(angle_point), sin(angle_point))*_radius)
	draw_polygon(points_arc, colors)


func set_tween(_object: Object = null, _key: = NodePath()) -> void:
	if move_range == 0:
		return
	move_range *= -1
	move_tween.interpolate_property(self, "position:x", position.x, position.x + move_range, move_speed, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
	move_tween.start()
