class_name Jumper
extends Area2D

signal captured
signal died

onready var trail: = $Trail/Points

var velocity: = Vector2(100, 0)
var jump_speed: = 1500
var target: Circle = null
var trail_lenght: = 25


func _on_VisibilityNotifier2D_screen_exited() -> void:
	# check if the player is not on a circle because it can exit the screen
	# when is turning arroud a circle that is partially out of the screen
	if !target:
		emit_signal('died')
		die()


func _on_Jumper_area_entered(area: Area2D) -> void:
	if Settings.enable_sound:
		$Capture.play()
	target = area
	velocity = Vector2.ZERO
	emit_signal("captured", area)


func _ready() -> void:
	$Sprite.material.set_shader_param('color', Settings.theme['player_body'])
	var trail_color: Color = Settings.theme['player_trail']
	trail.gradient.set_color(1, trail_color)
	trail_color.a = 0
	trail.gradient.set_color(0, trail_color)


func _unhandled_input(event: InputEvent) -> void:
	if target and (event is InputEventScreenTouch and event.is_pressed()) or event.is_action_pressed('jump'):
		jump()


func _physics_process(delta: float) -> void:
	if trail.points.size() > trail_lenght:
		trail.remove_point(0)

	trail.add_point(position)

	if target:
		transform = target.orbit_position.global_transform
	else:
		position += velocity*delta


func jump() -> void:
	if Settings.enable_sound:
		$Jump.play()
	target.implode()
	target = null
	velocity = transform.x*jump_speed


func die() -> void:
	print("Player DIE")
	target = null
	queue_free()
