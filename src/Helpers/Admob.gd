###################################
###  code for android ad mob module
###################################
class_name Admob, "res://assets/graphics/helpers/admob.png"
extends Node

# signals
signal banner_loaded
signal banner_failed_to_load(error_code)
signal insterstitial_failed_to_load(error_code)
signal interstitial_loaded
signal interstitial_closed
signal rewarded_video_loaded
signal rewarded_video_closed
signal rewarded(currency, ammount)
signal rewarded_video_left_application
signal rewarded_video_failed_to_load(error_code)
signal rewarded_video_opened
signal rewarded_video_started

# note : Avoid to publish ad ids in a public repo then "user://data/private/" folder
export var ad_ids_file: = "res://data/private/ad_ids.dat"
export var enable_ads: = true setget set_enable_ads
export var is_real: bool setget set_is_real
export var test_mode: bool setget set_test_mode
export (String, "Top", "Bottom") var banner_position: = "Bottom" setget set_banner_position
export var child_directed: = false setget set_child_directed
export var is_personalized: = true setget set_is_personalized
export (String, "G", "PG", "T", "MA") var max_ad_content_rate: = "G" setget set_max_ad_content_rate

# infos send by google when creating adds for an adMob app
# info are stored in a external (private) file, to avoid been published in a public repo
var banner_id: = ""
var interstitial_id: = ""
var rewarded_id: = ""

var is_interstitial_loaded: = false setget, get_is_interstitial_loaded
var is_rewarded_video_loaded: = false setget, get_is_rewarded_video_loaded

# "private" properties
var _admob = null
var _is_banner_on_top: = false

# test values for Ids
var _banner_id_test = "ca-app-pub-3940256099942544/6300978111"
var _interstitial_id_test = "ca-app-pub-3940256099942544/1033173712"
var _rewarded_id_test = "ca-app-pub-3940256099942544/5224354917"


# Signals callback
###########
func _on_admob_ad_loaded() -> void:
	emit_signal("banner_loaded")


func _on_admob_banner_failed_to_load(error_code: int) -> void:
	emit_signal("banner_failed_to_load", error_code)


func _on_insterstitial_failed_to_load(error_code: int) -> void:
	is_interstitial_loaded = false
	emit_signal("insterstitial_failed_to_load", error_code)


func _on_interstitial_loaded() -> void:
	is_interstitial_loaded = true
	emit_signal("interstitial_loaded")


func _on_interstitial_close() -> void:
	emit_signal("interstitial_closed")


func _on_rewarded_video_ad_loaded() -> void:
	is_rewarded_video_loaded = true
	emit_signal("rewarded_video_loaded")


func _on_rewarded_video_ad_closed() -> void:
	emit_signal("rewarded_video_closed")


func _on_rewarded(currency: String, amount: int) -> void:
	emit_signal("rewarded", currency, amount)


func _on_rewarded_video_ad_left_application() -> void:
	emit_signal("rewarded_video_left_application")


func _on_rewarded_video_ad_failed_to_load(error_code: int) -> void:
	is_rewarded_video_loaded = false
	emit_signal("rewarded_video_failed_to_load", error_code)


func _on_rewarded_video_ad_opened() -> void:
	emit_signal("rewarded_video_opened")


func _on_rewarded_video_started() -> void:
	emit_signal("rewarded_video_started")


# Methods override
###########
func _enter_tree() -> void:
	if !self.test_mode:
		enable_ads = load_add_ids()
	if init():
		ad_debug("Admob init success")
	else:
		enable_ads = false
		ad_debug("Admob init failed")


# Setters/Getter
###########
func set_enable_ads(new_val: bool) -> void:
	ad_debug("Admob set_enable_ads %s"%new_val)
	enable_ads = new_val
	if enable_ads:
		show_banner()
	else:
		hide_banner()


func set_is_real(new_val: bool) -> void:
	is_real = new_val
	# warning-ignore:return_value_discarded
	init()


func set_test_mode(new_val: bool) -> void:
	if new_val:
		banner_id = _banner_id_test
		interstitial_id = _interstitial_id_test
		rewarded_id = _rewarded_id_test
	else:
		# warning-ignore:return_value_discarded
		load_add_ids()
	# warning-ignore:return_value_discarded
	init()


func set_banner_position(pos: String) -> void:
	_is_banner_on_top = ("Top" == pos)


func set_child_directed(new_val: bool) -> void:
	child_directed = new_val
	# warning-ignore:return_value_discarded
	init()


func set_is_personalized(new_val: bool) -> void:
	is_personalized = new_val
	# warning-ignore:return_value_discarded
	init()


func set_max_ad_content_rate(new_val: String) -> void:
	if new_val != "G" and new_val != "PG" and new_val != "T" and new_val != "MA":
		max_ad_content_rate = "G"
		ad_debug("Invalid max_ad_content_rate, using 'G'")
	# warning-ignore:return_value_discarded
	init()


func get_is_interstitial_loaded() -> bool:
	if enable_ads and _admob:
		return is_interstitial_loaded
	return false


func get_is_rewarded_video_loaded() -> bool:
	if enable_ads and _admob:
		return is_rewarded_video_loaded
	return false


# Init/Load
###########
func init() -> bool:
	if Engine.has_singleton("AdMob"):
		_admob = Engine.get_singleton("AdMob")
		_admob.initWithContentRating(
			is_real, get_instance_id(), child_directed, is_personalized, max_ad_content_rate
		)
		ad_debug("AdMob: Java Singleton loaded")
		return true
	else:
		ad_debug("AdMob: Java Singleton not loaded")
		return false


func load_banner() -> void:
	if enable_ads and _admob:
		ad_debug("AdMob: load_banner call (Top = %s)"%_is_banner_on_top)
		_admob.loadBanner(banner_id, _is_banner_on_top)


func load_interstitial() -> void:
	if enable_ads and _admob:
		ad_debug("AdMob: loadInterstitial call")
		_admob.loadInterstitial(interstitial_id)


func load_rewarded_video() -> void:
	if enable_ads and _admob:
		ad_debug("AdMob: loadInterstitial call")
		_admob.loadRewardedVideo(rewarded_id)


# show/hide
###########
func show_banner() -> void:
	ad_debug("AdMob: show_banner (ads enabled=%s)"%enable_ads)
	if enable_ads and _admob:
		_admob.showBanner()


func hide_banner() -> void:
	ad_debug("AdMob: hide_banner (ads enabled=%s)"%enable_ads)
	if _admob:
		_admob.hideBanner()


func show_interstitial() -> void:
	if enable_ads and _admob:
		_admob.showInterstitial()
		is_interstitial_loaded = false


func show_rewarded_video() -> void:
	if enable_ads and _admob:
		_admob.showRewardedVideo()
		is_rewarded_video_loaded = false


# Helpers
###########
func banner_resize() -> void:
	if enable_ads and _admob:
		_admob.resize()


func get_banner_dimension() -> Vector2:
	if enable_ads and _admob:
		return Vector2(_admob.getBannerWidth(), _admob.getBannerHeight())
	return Vector2()


func ad_debug(text: String) -> void:
	print(text)


func load_add_ids() -> bool:
	var f: = File.new()
	if f.file_exists(ad_ids_file):
		if OK == f.open(ad_ids_file, File.READ):
			banner_id = f.get_line()
			interstitial_id = f.get_line()
			#Not used rewarded_id  = f.get_line()

			f.close()
			ad_debug("ids for admob loaded")
			return true
		else:
			ad_debug("Error opening ad_ids data file:%s"%ad_ids_file)
	return false
