extends CanvasLayer

onready var tween: = $Tween


func _on_TextEdit_meta_clicked(meta: String) -> void:
	# warning-ignore:return_value_discarded
	OS.shell_open(meta)


func appear() -> void:
	get_tree().call_group("buttons", "set_disabled", false)
	tween.interpolate_property(self, "offset:x", 500, 0, 0.5, Tween.TRANS_BACK, tween.EASE_IN_OUT)
	tween.start()


func disappear() -> void:
	get_tree().call_group("buttons", "set_disabled", true)
	tween.interpolate_property(self, "offset:x", 0, 500, 0.5, Tween.TRANS_BACK, tween.EASE_IN_OUT)
	tween.start()
