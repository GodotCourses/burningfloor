# NOTE:
# ajouter class_name Screen et déclarer les variables *screen* comme objet de type Screen empeche l'initialisation de $SettingsScreen,
# qui sera null dans la function _ready et donc l'appel change_screen($TitleScreen) ne fonctionnera pas

extends Node

signal start_game

var current_screen = null
var sound_buttons = {
	true: preload("res://assets/graphics/user_interface/buttons/audioOn.png"),
	false: preload("res://assets/graphics/user_interface/buttons/audioOff.png")
}
var music_buttons = {
	true: preload("res://assets/graphics/user_interface/buttons/musicOn.png"),
	false: preload("res://assets/graphics/user_interface/buttons/musicOff.png")
}

func _on_Button_pressed(button: BaseButton) -> void:
	if Settings.enable_sound:
		$Click.play()
	match button.name:
		"Ads":
			Settings.enable_ads = !Settings.enable_ads
			if Settings.enable_ads:
				button.text = "Disable Ads"
			else:
				button.text = "Enable Ads"
		"Home":
			change_screen($TitleScreen)
		"Play":
			change_screen(null)
			yield (get_tree().create_timer(0.5), "timeout")
			emit_signal("start_game")
		"Settings":
			change_screen($SettingsScreen)
		"About":
			change_screen($AboutScreen)
		"Sound":
			Settings.enable_sound = !Settings.enable_sound
			button.texture_normal = sound_buttons[Settings.enable_sound]
		"Music":
			Settings.enable_music = !Settings.enable_music
			button.texture_normal = music_buttons[Settings.enable_music]


func _ready() -> void:
	register_buttons()
	change_screen($TitleScreen)


func register_buttons() -> void:
	var buttons: = get_tree().get_nodes_in_group("buttons")

	for button in buttons:
		print("button pressed:" + button.name + " connected")
		button.connect("pressed", self, "_on_Button_pressed", [button])
		match button.name:
			"Ads":
				if Settings.enable_ads:
					button.text = "Disable Ads"
				else:
					button.text = "Enable Ads"
			"Sound":
				button.texture_normal = sound_buttons[Settings.enable_sound]
			"Music":
				button.texture_normal = music_buttons[Settings.enable_music]


func change_screen(new_screen) -> void:
	if current_screen:
		if current_screen.name == "SettingsScreen":
			Settings.save_settings()
		current_screen.disappear()
		yield (current_screen.tween, "tween_completed")
	current_screen = new_screen
	if current_screen:
		current_screen.appear()
		yield (current_screen.tween, "tween_completed")


func game_over(score: int, highscore: int) -> void:
	var score_box: = $GameOverScreen/MarginContainer/VBoxContainer/VBoxContainer
	score_box.get_node('Score').text = "Score: %s"%str(score)
	score_box.get_node('Best').text = "Best: %s"%str(highscore)
	change_screen($GameOverScreen)
