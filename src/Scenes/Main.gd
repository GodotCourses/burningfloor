extends Node

var Circle: PackedScene = preload('res://src/Actors/Circle.tscn')
var Jumper: PackedScene = preload('res://src/Actors/Jumper.tscn')
var player: Jumper
var score: = 0 setget set_score
var highscore: = 0
var new_highscore: = false
var level: = 0
var bonus: = 0 setget set_bonus
var num_circles_touched: = 0


func _on_Settings_ads_status_changed(status: bool) -> void:
	$Admob.enable_ads = status


func _on_Jumper_died() -> void:
	if score > highscore:
		highscore = score
		save_score()
	get_tree().call_group("circles", "implode")
	$Screens.game_over(score, highscore)
	$HUD.hide()
	if Settings.enable_music:
		fade_music()
	$Admob.show_interstitial()


func _on_Jumper_captured(object: Circle) -> void:
	$Camera2D.position = object.position
	object.capture(player)
	call_deferred("spaw_circle")
	set_score(score + 1*bonus)
	set_bonus(bonus + 1)
	num_circles_touched += 1
	if num_circles_touched > 0 and num_circles_touched%Settings.circles_per_level == 0:
		level += 1
		$HUD.show_message("Level %s"%str(level))


func _ready() -> void:
	randomize()
	load_score()
	$HUD.hide()
	$Admob.load_banner()
	$Admob.load_interstitial()
	# warning-ignore:return_value_discarded
	$Admob.connect("interstitial_closed", $Admob, "show_banner")
	# warning-ignore:return_value_discarded
	Settings.connect('ads_status_changed', self, '_on_Settings_ads_status_changed')


func new_game() -> void:
	print("Game Started")
	$Admob.hide_banner()
	set_score(0)
	set_bonus(0)
	num_circles_touched = 0
	level = 1
	new_highscore = false
	$Camera2D.position = $StartPosition.position
	player = Jumper.instance()
	player.position = $StartPosition.position
	add_child(player)
	# warning-ignore:return_value_discarded
	player.connect('captured', self, "_on_Jumper_captured")
	# warning-ignore:return_value_discarded
	player.connect('died', self, "_on_Jumper_died")
	spaw_circle($StartPosition.position)
	$HUD.show()
	$HUD.show_message('Go !')
	if Settings.enable_music:
		$Music.volume_db = 0
		$Music.play()


func spaw_circle(_position: = Vector2.ZERO) -> void:
	var c = Circle.instance()
	if !_position:
		var x: = rand_range(-150, 150)
		var y: = rand_range(-500, -400)
		_position = player.position + Vector2(x, y)
	add_child(c)
	c.connect("orbit_completed", self, "set_bonus", [1])
	c.init(_position, level)


func set_bonus(value: int) -> void:
	bonus = value
	$HUD.update_bonus(bonus)


func set_score(value: int) -> void:
	$HUD.update_score(score, value)
	score = value
	if score > highscore and !new_highscore:
		new_highscore = true
		$HUD.show_message("HighScore !")


func save_score() -> void:
	var f: = File.new()
	if OK == f.open(Settings.score_file, File.WRITE):
		f.store_32(highscore)
		f.close()


func load_score() -> void:
	var f: = File.new()
	if f.file_exists(Settings.score_file):
		if OK == f.open(Settings.score_file, File.READ):
			highscore = f.get_32()
			f.close()
		else:
			print("Error opening highscore save file:%s"%Settings.score_file)


func fade_music() -> void:
	$MusicFade.interpolate_property($Music, "volume_db", 0, -50, 1.0, Tween.TRANS_SINE, Tween.EASE_IN)
	$MusicFade.start()
	yield ($MusicFade, 'tween_all_completed')
	$Music.stop()
