# Burning Floor

## Description

Jeu créé dans le cadre du tuto [circle Jump](http://kidscancode.org/godot_recipes/games/circle_jump).
Disponible sur les plateformes PC et Android.

## Objectifs

Passer d'un cercle à l'autre le plus rapidement possible.

## Mouvements et actions du joueur

- Version PC:
  - Quitter le circle courant (sauter):
    - touche espace ou clic gauche de la souris
  - Quitter le jeu: clic sur la croix.
- Version mobile
  - Quitter le circle courant (sauter):
    - toucher l'écran.
  - Quitter le jeu: bouton retour.

## Interactions

- Le joueur
  - perd la partie s'il saute sans toucher le cercle suivant.
  - perd la partie s'il ne saute pas à temps (certains cercles uniquement)
  - cumule les bonus s'il saute avant d'avoir fait un tour complet.

## Copyrights

Codé en GDscript en utilisant le moteur de jeu Godot Engine.
Développé en utilisant principalement l'éditeur de code intégré et/ou Visual Studio Code.

-----------------
(C) 2020 GameaMea (http://www.gameamea.com)

=================================================

## Description

Game created as part of the tutorial [circle Jump](http://kidscancode.org/godot_recipes/games/circle_jump)
Available on PC and Android platforms.

## Goals

Go from one circle to another in a given time.

## Movements and actions of the player

- PC version:
  - Leave the current circle (jump):
    - space key or left mouse click
  - Quit the game: click on the cross.git
- Mobile version
  - Leave the current circle (jump):
    - touch the screen.
  - Quit the game: back button.

## Interactions

- The player
  - loses the game if he jumps without touching the next circle.
  - loses the game if he does not jump in time (some circles only)
  - accumulates bonuses if he jumps before having completed a full turn.

## Copyrights

Coded in GDscript using the Godot Engine game engine.
Developed using mainly the integrated code editor and/or Visual Studio Code.

-----------------
(C) 2020 GameaMea (http://www.gameamea.com)
